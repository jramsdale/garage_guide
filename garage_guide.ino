#include <Adafruit_NeoPixel.h>

// ---------------------------------------------------------------------------
// Garage distance detector
// ---------------------------------------------------------------------------

#include <avr/power.h>
#include <Chrono.h>
#include <NewPing.h>
#include <movingAvg.h>

#define ECHO_PIN     0  // Arduino pin tied to echo pin on the ultrasonic sensor.
#define NEOPIXEL_PIN 1
#define TRIGGER_PIN  2  // Arduino pin tied to trigger pin on the ultrasonic sensor.

#define MIN_DISTANCE 10 // Minimum distance
#define MAX_DISTANCE 150 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.
#define LED_THRESHOLD 9 // Zero based!
#define LED_STRAND_LENGTH 20

#define TIMEOUT 60000

Adafruit_NeoPixel strip = Adafruit_NeoPixel(LED_STRAND_LENGTH, NEOPIXEL_PIN, NEO_GRB + NEO_KHZ800);
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.
Chrono timer;
movingAvg avgDistance(10);

bool isActive = false;
unsigned int distance;
unsigned int led_count;

uint32_t black = strip.Color(0, 0, 0);
uint32_t green = strip.Color(0, 255, 0);
uint32_t red = strip.Color(255, 0, 0);

void setup() {
  // Set CPU to 16 MHz
  if (F_CPU == 16000000) {
    clock_prescale_set (clock_div_1);
  }
  avgDistance.begin();
  strip.begin();
  strip.setBrightness(127);
  strip.show(); // Initialize all pixels to 'off'
}

void loop() {
  delay(30); // Wait 50ms between pings (about 20 pings/sec). 29ms should be the shortest delay between pings.

  distance = sonar.ping_cm();
  avgDistance.reading(distance);

  // If timer has passed timeout then turn off strip
  if (timer.hasPassed(TIMEOUT)) {
    isActive = false;
    leds_off();
  }

  // If avgDistance and distance are equal then car hasn't moved
  // If car has moved or strip is active:
  int distanceDiff = avgDistance.getAvg() - distance;
  if (abs(distanceDiff) > 3 || isActive) {
    if (!isActive) {
      isActive = true;
      timer.restart();
    }
    light_leds(map(avgDistance.getAvg(), MIN_DISTANCE, MAX_DISTANCE, LED_STRAND_LENGTH - 1, 0));
  }
}

void leds_off() {
  for (int i = 0; i < LED_STRAND_LENGTH; i++) {
    strip.setPixelColor(i, black);
  }
  strip.show();
}

void light_leds(int led_count) {
  int green_length = min(led_count, LED_THRESHOLD);
  int i;
  
  // Set green pixels
  for (i=0; i <= green_length; i++) {
    strip.setPixelColor(i, green);
  }

  // Set red pixels
  if (led_count >= LED_THRESHOLD) {
    for (; i < led_count; i++) {
      strip.setPixelColor(i, red);
    }
    strip.setPixelColor(LED_STRAND_LENGTH - 1, red);
  } else {
    strip.setPixelColor(LED_STRAND_LENGTH - 1, black);
  }

  // Set black pixels
  for (; i < LED_STRAND_LENGTH - 1; i++) {
    strip.setPixelColor(i, black);
  }
  
  strip.show();
}

